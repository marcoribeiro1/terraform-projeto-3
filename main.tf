# Configure the AWS Provider
provider "aws" {
  region     = var.aws_region
  access_key = var.access_key
  secret_key = var.secret_key
}

resource "aws_instance" "instancia_C" {
  ami            = data.aws_ami.amazon_linux.id
  instance_type = "t3a.micro"
}
